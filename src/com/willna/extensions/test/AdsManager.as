package com.willna.extensions.test
{
	import com.willna.utils.PropertiesLoader;
	
	import flash.filesystem.File;

	public class AdsManager 
	{
		public static var ADFONIC_BANNER_ID:String = "";
		public static var ADFONIC_FULLSCREEN_ID:String = "";
		
		public static var ADIQUITY_SITE_ID:String = "";
		
		public static var ADMOB_PUBLISHER_ID:String = "";
		
		public static var INMOBI_APP_ID:String = "";
		public static var INMOBI_SLOT_ID:String = "0";
		
		public static var INNERACTIVE_APP_ID:String = "";
		
		public static var SMAATO_PUBLISHER_ID:String = "";
		public static var SMAATO_ADSPACE_ID:String = "";
		
		
		public function AdsManager( )
		{
			
			
		}
		
		public static function init( ):void
		{
			var properties:PropertiesLoader = new PropertiesLoader( File.applicationDirectory.resolvePath("settings.ini") );
			
			if (properties.getValue("adfonic_banner_id") != null)
				ADFONIC_BANNER_ID = properties.getValue("adfonic_banner_id");
			if (properties.getValue("adfonic_interstitial_id") != null)
				ADFONIC_FULLSCREEN_ID = properties.getValue("adfonic_interstitial_id");
			
			if (properties.getValue("adiquity_site_id") != null)
				ADIQUITY_SITE_ID = properties.getValue("adiquity_site_id");
			
			if (properties.getValue("admob_publisher_id") != null)
				ADMOB_PUBLISHER_ID = properties.getValue("admob_publisher_id");
			
			if (properties.getValue("inmobi_app_id") != null)
				INMOBI_APP_ID = properties.getValue("inmobi_app_id");
			if (properties.getValue("inmobi_slot_id") != null)
				INMOBI_SLOT_ID = properties.getValue("inmobi_slot_id");
			
			if (properties.getValue("inneractive_app_id") != null)
				INNERACTIVE_APP_ID = properties.getValue("inneractive_app_id");
			
			if (properties.getValue("smaato_publisher_id") != null)
				SMAATO_PUBLISHER_ID = properties.getValue("smaato_publisher_id");
			if (properties.getValue("smaato_adspace_id") != null)
				SMAATO_ADSPACE_ID = properties.getValue("smaato_adspace_id");			
		}
		
	}
}