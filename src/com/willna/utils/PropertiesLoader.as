package com.willna.utils
{
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	
	import mx.utils.StringUtil;
	
	public class PropertiesLoader
	{
		private var properties:Object;
		
		public function PropertiesLoader(file:File)
		{
			properties = new Object();
			if (file.exists)
			{
				var stream:FileStream = new FileStream();
				stream.open(file, FileMode.READ);
				var str:String = stream.readUTFBytes(stream.bytesAvailable);
				stream.close();
				str = str.replace(File.lineEnding, "\n");
				
				var lines:Array = str.split( "\n" );
				
				for each ( var line:String in lines )
				{
					line = StringUtil.trim(line);
					if (line.length < 3)	continue;
					if (line.charAt(0) == "#")	continue;
					
					var pair:Array = line.split( "=" );
					if (pair.length < 2)	continue;
					
					var key:String = StringUtil.trim( pair.shift() );
					properties[key] = StringUtil.trim( pair.join("=") );
				}
			}
		}
		
		public function getValue(key:String):String
		{
			if (properties && properties.hasOwnProperty(key))
				return properties[key];
			else
				return null;
		}
	}
}